# eZ Platform Custom Tag Tooltips

This bundle provides on-hover tooltips for eZ Platform's rich text editor custom tag icons as well as the stock icon buttons.

## Installation

1. Require via composer
    ```bash
    composer require contextualcode/ezplatform-alloyeditor-customtag-tooltips
    ```

2. Clear browser caches and enjoy!

## Usage

**Hover over any custom tag or stock icon button in the rich text Online Editor toolbar:**  
    ![open_modal](https://gitlab.com/contextualcode/ezplatform-alloyeditor-customtag-tooltips/raw/master/doc/images/custom-tag-tooltip.png)    
    
**They also work for the stock icons in the rich text editor:**    
    ![edit_source_modal](https://gitlab.com/contextualcode/ezplatform-alloyeditor-customtag-tooltips/raw/master/doc/images/stock-button-tooltip.png)
